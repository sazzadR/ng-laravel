<?php

namespace App\Http\Controllers;

use JWTAuth;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class AuthenticateController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return string
     */
    public function index()
    {
        return 'Auth index';
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            // Verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'Invalid credentials'], 401);
            }
        } catch (JWTException $e) {
            // Something went wrong
            return response()->json(['error' => 'Could not create token'], 500);
        }

        // If no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }

    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['error' => 'User not found'], 404);
            }
        } catch (TokenExpiredException $expiredException) {
            return response()->json(['error' => 'Token expired'], $expiredException->getStatusCode());
        } catch (TokenInvalidException $invalidException) {
            return response()->json(['error' => 'Token invalid'], $invalidException->getStatusCode());
        } catch (JWTException $JWTException) {
            return response()->json(['Token absent'], $JWTException->getStatusCode());
        }

        // The token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }
}
