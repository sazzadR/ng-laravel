<?php

namespace App\Http\Controllers;

use Response;
use App\Joke;
use Illuminate\Http\Request;

class JokesController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $searchItem = $request->input('search');
        $limit = $request->input('limit') ? $request->input('limit') : 5;

        $jokes = Joke::orderBy('id', 'desc')->where(function ($query) use ($searchItem) {
            if ($searchItem) {
                $query->where('body', 'LIKE', "%$searchItem%");
            }
        })->with([
            'User' => function ($query) {
                $query->select('id', 'name');
            }
        ])->select('id', 'body', 'user_id')->paginate($limit);

        return Response::json([
            'message' => $this->transformCollection($jokes)
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if (!$request->body or !$request->user_id) {
            return Response::json([
                'error' => [
                    'message' => 'Please provide both body and user id'
                ]
            ], 422);
        }

        $joke = Joke::create($request->all());

        return Response::json([
            'message' => 'Joke created successfully',
            'data' => $this->transform($joke)
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $joke = Joke::with([
            'User' => function ($query) {
                $query->select('id', 'name');
            }
        ])->select('id', 'body', 'user_id')->find($id);

        return Response::json([
            'message' => $this->transform($joke)
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        if ($request->body && $request->user_id && $id) {
            $joke = Joke::find($id);
            $joke->body = $request->body;
            $joke->user_id = $request->user_id;
            $joke->save();

            return Response::json([
                'message' => 'Joke saved successfully'
            ], 200);
        }

        return Response::json([
            'error' => [
                'message' => 'Provide all necessary parameters'
            ]
        ], 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        if ($id) {
            $deleteJoke = Joke::destroy($id);

            if ($deleteJoke) {
                return Response::json([
                    'message' => 'Joke deleted successfully'
                ], 200);
            }

            return Response::json([
                'error' => [
                    'message' => 'There is something wrong'
                ]
            ], 304);
        }

        return Response::json([
            'error' => [
                'message' => 'Provide all necessary parameters'
            ]
        ], 422);
    }

    private function transformCollection($jokes)
    {
        $jokesArray = $jokes->toArray();

        return [
            'total' => $jokesArray['total'],
            'per_page' => $jokesArray['per_page'],
            'current_page' => $jokesArray['current_page'],
            'last_page' => $jokesArray['last_page'],
            'next_page_url' => $jokesArray['next_page_url'],
            'prev_page_url' => $jokesArray['prev_page_url'],
            'to' => $jokesArray['to'],
            'data' => array_map([$this, 'transform'], $jokesArray['data'])
        ];
    }

    private function transform($joke)
    {
        return [
            'joke_id' => $joke['id'],
            'joke' => $joke['body'],
            'submitted_by' => $joke['user']['name']
        ];
    }
}
