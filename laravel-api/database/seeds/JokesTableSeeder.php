<?php

use App\Joke;
use Illuminate\Database\Seeder;

class JokesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (range(1, 30) as $index) {
            Joke::create([
                'body' => $faker->paragraph(),
                'user_id' => $faker->numberBetween(1, 5)
            ]);
        }
    }
}
