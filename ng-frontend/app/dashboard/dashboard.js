'use strict';

angular.module('myApp.dashboard', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/dashboard', {
        templateUrl: 'dashboard/dashboard.html',
        controller: 'DashboardCtrl',
        authenticated: true
    });
}])

.controller('DashboardCtrl', ['$rootScope', '$scope', '$http', '$log', 'growl', 'CONFIG', function ($rootScope, $scope, $http, $log, growl, CONFIG) {
    // Properties
    angular.extend($scope, {
        recordCache: {},
        newJoke: {}
    });

    // Functions
    angular.extend($scope, {
        getJokes: function (urlWithPage = null) {

            if (urlWithPage) {
                var url = urlWithPage;
            } else {
                var url = CONFIG.apiUrl + 'api/v1/jokes';
            }

            $http({
                method: 'GET',
                url: url
            }).then(function (responseSuccess) {
                $scope.jokesData = responseSuccess.data.message;
                $scope.jokes = responseSuccess.data.message.data;
            }, function (responseError) {
                $scope.error = responseError;
            });
        },

        addJoke: function () {
            $http({
                method: 'POST',
                url: CONFIG.apiUrl + 'api/v1/jokes',
                data: {
                    body: $scope.newJoke.joke,
                    user_id: $rootScope.currentUser.id
                }
            }).then(function (response) {
                // Update DOM with new jokes
                $scope.getJokes();
                $scope.newJoke.joke = null;
            });
        },

        editJoke: function (joke) {
            $scope.recordCache = angular.copy(joke);

            joke.editing = true;
        },

        updateJoke: function (joke) {
            $http({
                method: 'PUT',
                url: CONFIG.apiUrl + 'api/v1/jokes/' + joke.joke_id,
                data: {
                    body: joke.joke,
                    user_id: $rootScope.currentUser.id
                }
            }).then(function (responseSuccess) {
                // $log.log(responseSuccess);
            }, function (responseError) {
                $log.log(responseSuccess);
            });

            joke.editing = false;
        },

        cancelEditJoke: function (index, joke) {
            $scope.jokes[index] = $scope.recordCache;
            $scope.recordCache = {};

            joke.editing = false;
        },

        deleteJoke: function (joke) {
            $http.delete(CONFIG.apiUrl + 'api/v1/jokes/' + joke.joke_id).then(function (responseSuccess) {
                $scope.getJokes();
            }, function (responseError) {
                $log.log(responseError);
            });
        }
    });

    // Get all jokes initially
    $scope.getJokes();

}]);