angular.module('myApp').factory('authInterceptor', ['$q', '$window', '$location', function ($q, $window, $location) {
        return {
            request: function (config) {
                config.headers = config.headers || {};
                config.headers.Authorization = 'Bearer ' + $window.localStorage.getItem('jwt'); // add your token from your service or whatever
                return config;
            },
            response: function (response) {
                return response || $q.when(response);
            },
            responseError: function (rejection) {
                // Clear local storage and redirect to login page
                $window.localStorage.clear();
                $location.path('/auth');
            }
        };
    }
]);

// Register the previously created authInterceptor
app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
});
