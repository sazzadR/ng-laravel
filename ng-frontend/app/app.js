'use strict';

// Declare app level module which depends on views, and components
var app =
angular.module('myApp', [
    'ngRoute',
    'ngCookies',
    'angular-growl',
    'myApp.auth',
    'myApp.dashboard'
]).
config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider.otherwise({ redirectTo: '/auth' });
}]).
run(['$rootScope', '$location', '$log', function ($rootScope, $location, $log) {
    $rootScope.$on('$routeChangeStart', function (event, next) {

        if (angular.isDefined(next.$$route)) {
            if (next.$$route.authenticated) {
                if (!localStorage.jwt && !localStorage.user) {
                    $location.path('/auth');
                }
            }
        }
    });
}]);