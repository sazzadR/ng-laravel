'use strict';

app.factory('AuthFactory', ['$rootScope', '$http', '$location', '$log', 'CONFIG', function ($rootScope, $http, $location, $log, CONFIG) {
    var AuthFactory = {};

    AuthFactory.attemptLogin = function (credentials) {
        $http({
            method: 'POST',
            url: CONFIG.apiUrl + 'api/v1/authenticate',
            data: credentials
        }).then(function (responseSuccess) {
            if (angular.isDefined(responseSuccess)) {
                var jwt = responseSuccess.data.token;
                localStorage.setItem('jwt', jwt);

                $http.get(CONFIG.apiUrl + '/api/v1/authenticate/user').then(function (responseSuccess) {

                    $rootScope.currentUserAfterLogin = responseSuccess.data.user;

                    var user = JSON.stringify(responseSuccess.data.user);
                    localStorage.setItem('user', user);

                    $location.path('/dashboard');
                });
            }
        }, function (responseError) {
            $log.log(responseError);
        })
    };

    AuthFactory.getUserObject = function () {
        return angular.fromJson(localStorage.getItem('user'));
    };

    AuthFactory.isLoggedIn = function () {
        return ((localStorage.getItem('user') !== null) && (localStorage.getItem('user') !== null));
    };

    AuthFactory.doLogout = function () {
        localStorage.clear();
        $rootScope.currentUser = null;
        $location.path('/');
    }

    return AuthFactory;
}]);
