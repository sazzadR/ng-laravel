'use strict';

angular.module('myApp.auth', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/auth', {
        templateUrl: 'auth/auth.html',
        controller: 'AuthCtrl',
        authenticated: false
    });
}])

.controller('AuthCtrl', ['$scope', '$http', '$location', '$log', 'CONFIG', 'AuthFactory', function ($scope, $http, $location, $log, CONFIG, AuthFactory) {
    angular.extend($scope, {
        loginUser: function () {
            var credentials = {
                email: $scope.email,
                password: $scope.password
            };

            AuthFactory.attemptLogin(credentials);
        }
    });
}]);
