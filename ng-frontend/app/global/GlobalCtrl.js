app.controller('GlobalCtrl', ['$rootScope', '$scope', '$log', 'AuthFactory', function ($rootScope, $scope, $log, AuthFactory) {
    if (AuthFactory.isLoggedIn()) {
        $rootScope.currentUser = AuthFactory.getUserObject();
    }

    angular.extend($rootScope, {
        loggedIn: function () {
            return AuthFactory.isLoggedIn();
        },

        logout: function () {
            AuthFactory.doLogout();
        }
    });
}]);